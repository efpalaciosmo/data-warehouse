CREATE TABLE Location(
  locno VARCHAR(4),
  facno VARCHAR(4),
  locname VARCHAR(120) NOT NULL,
  CONSTRAINT LocNO PRIMARY KEY (locno),
  CONSTRAINT facnoFK FOREIGN KEY (facno) REFERENCES Facility
);

INSERT INTO Location(locno, facno, locname)
VALUES
    ('L100', 'F100',	'Locker room'),
    ('L101', 'F100',    'Plaza'),
    ('L102', 'F100',	'Vehicle gate'),
    ('L103', 'F101',	'Locker room'),
    ('L104', 'F100',	'Ticket Booth'),
    ('L105', 'F101',	'Gate'),
    ('L106', 'F100',	'Pedestrian gate')
