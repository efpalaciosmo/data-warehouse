CREATE TABLE Facility(
    facno VARCHAR(4) NOT NULL ,
    facname VARCHAR(120) NOT NULL ,
    CONSTRAINT FacNo PRIMARY KEY (facno),
    CONSTRAINT UniqueFacName UNIQUE (facname)
);

INSERT INTO Facility
    (facno, facname)
    VALUES('F100', 'Football stadium');

INSERT INTO Facility
    (facno, facname)
    VALUES ('F101',	'Basketball arena');

INSERT INTO Facility
    (facno, facname)
     VALUES('F102',	'Baseball field');
INSERT INTO Facility
    (facno, facname)
    VALUES('F103', 'Recreation room');




